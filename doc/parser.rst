.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.markup.semtext -- Sly parser
.. :Created:   dom 15 gen 2017 14:20:49 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2017 Arstecnica s.r.l.
.. :Copyright: © 2018 Lele Gaifax
..

========
 Parser
========

.. automodule:: metapensiero.markup.semtext.parser
   :members:
