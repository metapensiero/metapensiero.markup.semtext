.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.markup.semtext -- Sly lexer
.. :Created:   dom 15 gen 2017 14:19:43 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2017 Arstecnica s.r.l.
.. :Copyright: © 2018 Lele Gaifax
..

=======
 Lexer
=======

.. automodule:: metapensiero.markup.semtext.lexer
   :members:
