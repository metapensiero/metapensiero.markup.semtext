.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.markup.semtext -- HTML parser documentation
.. :Created:   ven 21 apr 2017 14:29:52 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2017 Arstecnica s.r.l.
.. :Copyright: © 2018 Lele Gaifax
..

======
 HTML
======

.. automodule:: metapensiero.markup.semtext.html
   :members:
