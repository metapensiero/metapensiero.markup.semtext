.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.markup.semtext -- AST elements
.. :Created:   dom 15 gen 2017 14:18:17 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2017 Arstecnica s.r.l.
.. :Copyright: © 2018 Lele Gaifax
..

==============
 AST elements
==============

.. automodule:: metapensiero.markup.semtext.ast
   :members:
