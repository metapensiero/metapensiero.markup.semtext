.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.markup.semtext -- Visitor pattern
.. :Created:   dom 15 gen 2017 14:21:31 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2017 Arstecnica s.r.l.
.. :Copyright: © 2018 Lele Gaifax
..

=========
 Visitor
=========

.. automodule:: metapensiero.markup.semtext.visitor
   :members:
