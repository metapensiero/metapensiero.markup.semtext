.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.markup.semtext -- Documentation
.. :Created:   gio 08 dic 2016 17:39:46 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2016, 2017 Arstecnica s.r.l.
.. :Copyright: © 2018 Lele Gaifax
..

======================
 Simple Enough Markup
======================

The library implements a simplistic markup language suitable to be used in the
description fields and that can be rendered either in ``HTML`` or in the
reports.


.. automodule:: metapensiero.markup.semtext

.. toctree::

   ast
   lexer
   parser
   text
   visitor
   html
   quill
