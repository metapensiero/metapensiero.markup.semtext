.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.markup.semtext -- Text parser
.. :Created:   dom 02 apr 2017 14:06:37 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2017 Arstecnica s.r.l.
.. :Copyright: © 2018 Lele Gaifax
..

=============
 Text parser
=============

.. automodule:: metapensiero.markup.semtext.text
   :members:
