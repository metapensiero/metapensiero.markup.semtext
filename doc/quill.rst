.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.markup.semtext -- Quill related stuff documentation
.. :Created:   sab 29 lug 2017 11:30:23 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2017 Arstecnica s.r.l.
.. :Copyright: © 2018 Lele Gaifax
..

=======
 Quill
=======

.. automodule:: metapensiero.markup.semtext.quill
   :members:
